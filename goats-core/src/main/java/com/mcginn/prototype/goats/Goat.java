package com.mcginn.prototype.goats;

import java.io.Serializable;
import java.time.LocalDate;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class Goat implements Serializable {

	private static final long serialVersionUID = -662005206748089516L;
	
	private final String name;
	private final GoatGender gender;
	private final LocalDate birthDate;
	private final Breed primaryBreed;
	
	public Goat(String name, GoatGender gender, LocalDate birthDate, Breed primaryBreed) {
		this.name = name;
		this.gender = gender;
		this.birthDate = birthDate;
		this.primaryBreed = primaryBreed;
	}

	public String getName() {
		return name;
	}

	public GoatGender getGender() {
		return gender;
	}

	public LocalDate getBirthDate() {
		return birthDate;
	}

	public Breed getPrimaryBreed() {
		return primaryBreed;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(getName()).append(getBirthDate()).toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if(obj == this) {
			return true;
		}
		
		if(obj == null || !(obj instanceof Goat)) {
			return false;
		}
		
		Goat other = (Goat)obj;
		
		return new EqualsBuilder().append(this.getName(), other.getName())
				.append(this.getBirthDate(), other.getBirthDate()).isEquals();
	}

	@Override
	public String toString() {
		return String.format("%s is a %s %s born on %tD", getName(), getPrimaryBreed(), getGender(), getBirthDate());
	}
}
