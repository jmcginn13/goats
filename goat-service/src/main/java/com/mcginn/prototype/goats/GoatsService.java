package com.mcginn.prototype.goats;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mcginn.prototype.goats.db.GoatsDb;

@RestController
public class GoatsService {

	@Autowired
	private GoatsDb goatsDb;
	
	@RequestMapping(value="/goats", method=RequestMethod.GET, produces="application/json")
	public List<Goat> allGoats() {
		return this.goatsDb.getAllGoats();
	}
}
