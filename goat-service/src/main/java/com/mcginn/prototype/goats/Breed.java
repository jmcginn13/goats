package com.mcginn.prototype.goats;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class Breed implements Serializable {

	private static final long serialVersionUID = -6491094113212412563L;
	private final String breedName;

	public Breed(String breedName) {
		this.breedName = breedName;
	}

	public String getBreedName() {
		return breedName;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(this.getBreedName()).toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if(obj == this) {
			return true;
		}
		
		if(obj == null || !(obj instanceof Breed)) {
			return false;
		}
		
		Breed other = (Breed)obj;
		return new EqualsBuilder().append(this.getBreedName(), other.getBreedName()).isEquals();
	}

	@Override
	public String toString() {
		return String.format("%s", this.getBreedName());
	}
}
