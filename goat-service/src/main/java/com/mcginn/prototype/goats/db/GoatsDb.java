package com.mcginn.prototype.goats.db;

import java.util.List;

import com.mcginn.prototype.goats.Goat;

public interface GoatsDb {

	List<Goat> getAllGoats();
}
