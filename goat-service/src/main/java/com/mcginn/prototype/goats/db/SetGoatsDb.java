package com.mcginn.prototype.goats.db;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Component;

import com.mcginn.prototype.goats.Breed;
import com.mcginn.prototype.goats.Goat;
import com.mcginn.prototype.goats.GoatGender;

@Component
public class SetGoatsDb implements GoatsDb {

	private final Set<Goat> goats;
	
	public SetGoatsDb() {
		this.goats = new HashSet<Goat>();
		this.goats.add(new Goat("Ellie", GoatGender.Doe, LocalDate.of(2014, Month.APRIL, 11), new Breed("Nubian")));
		this.goats.add(new Goat("Willow", GoatGender.Doe, LocalDate.of(2014, Month.MAY, 4), new Breed("Nubian")));
		this.goats.add(new Goat("Thor", GoatGender.Wether, LocalDate.of(2014, Month.JUNE, 4), new Breed("Nubian")));
		this.goats.add(new Goat("Loki", GoatGender.Wether, LocalDate.of(2014, Month.JUNE, 4), new Breed("Nubian")));
		this.goats.add(new Goat("Fleetwood Mac", GoatGender.Buck, LocalDate.of(2015, 2, 15), new Breed("Nubian")));
	}

	@Override
	public List<Goat> getAllGoats() {
		List<Goat> goats = new ArrayList<Goat>(this.goats);
		Collections.sort(goats, new Comparator<Goat>() {
			@Override
			public int compare(Goat o1, Goat o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});
		return goats;
	}
}
